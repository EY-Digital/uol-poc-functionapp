const apikey = "2b18099ca33d262c10601b8cd6fbda02";
const token = "15af4e61d125b5a711648f6555d989aae5a020a322164fb39989bfae81ceb42a";
const cardid_bin = "5bee3b9394a0b146259ce346";
const listid_todo = "5beaaf19a2b06180b474aa74";
const secret = "mg5KZBhQbqHSybfG6g2mhMOJaHJcWtFSQCIsOLO36Ymh7MN9At14I4nGNUW5biUU";
const apihost = "https://api.trello.com/1";

const request = require("request-promise");
let total = 0;
const threshold = 100;

function getBinRemaining() {
    return {
        method: 'GET',
        uri: `${apihost}/cards/${cardid_bin}/?key=${apikey}&token=${token}`,
        json: true
    };
}

function updateBinRemaining(total) {
    let desc = total.toString();
    return {
        method: 'PUT',
        uri: `${apihost}/cards/${cardid_bin}?desc=${desc}&key=${apikey}&token=${token}`,
        json: true
    };
}

function addTask() {
    let name = encodeURIComponent("Empty the garbage bin 1");
    return {
        method: 'POST',
        uri: `${apihost}/cards?idList=${listid_todo}&name=${name}&key=${apikey}&token=${token}`,
        json: true
    }
}

module.exports = async function (context, req) {
    context.log('JavaScript Timer trigger function processed a request.');
    return request(getBinRemaining()).then( card => {
        let remain = parseInt(card.desc);
        context.log(remain);

        if(remain >= threshold){
            // Stop processing
            throw new Error("Bin is not cleared yet");
        }

        let garbage = Math.ceil(Math.random() * 10) * Math.ceil(Math.random() * 10); // 1-100
        total = remain + garbage;
        return request(updateBinRemaining(total));
    }).then(res => {
        if(total >= threshold){
            return request(addTask());
        }
        context.done();
    }).catch(e => {
        context.log(e.message);
    });
};
