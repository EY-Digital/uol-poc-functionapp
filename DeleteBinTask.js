const apikey = "2b18099ca33d262c10601b8cd6fbda02";
const token = "15af4e61d125b5a711648f6555d989aae5a020a322164fb39989bfae81ceb42a";
const listid_todo = "5beaaf19a2b06180b474aa74";
const cardid_bin = "5bee3b9394a0b146259ce346";
const secret = "mg5KZBhQbqHSybfG6g2mhMOJaHJcWtFSQCIsOLO36Ymh7MN9At14I4nGNUW5biUU";
const apihost = "https://api.trello.com/1/";

const request = require("request-promise");
const regexp = RegExp('^Empty\\sthe\\sgarbage\\sbin\\s\\d+');
const threshold = 100;

function getBinRemaining() {
    return {
        method: 'GET',
        uri: `${apihost}/cards/${cardid_bin}/?key=${apikey}&token=${token}`,
        json: true
    };
}

function getAllTasks() {
    return {
        method: 'GET',
        uri: `${apihost}/lists/${listid_todo}/cards?key=${apikey}&token=${token}`,
        json: true
    };
}

function emptyBinRemaining() {
    return {
        method: 'PUT',
        uri: `${apihost}/cards/${cardid_bin}?desc=0&key=${apikey}&token=${token}`,
        json: true
    };
}

module.exports = async function (context, req) {
    context.log('JavaScript Timer trigger function processed a request.');
    // Empty the bin when both of following criteria meet
    // 1. Remaining >= 100
    // 2. Task card is not present
    return request(getBinRemaining()).then( card => {
        let remain = parseInt(card.desc);
        if(remain < threshold) {
            throw new Error('Bin is still not full');
        }
        return request(getAllTasks());
    }).then( cards => {
        for(let i in cards){
            context.log(cards[i].name);
            if(regexp.test(cards[i].name)) {
                throw new Error('Bin is still not cleared');
            }
        }
        return request(emptyBinRemaining());
    }).then( res => {
        context.log('Bin is cleared');
    }).catch( e => {
        context.log(e.message);
    });
};