let apikey = "2b18099ca33d262c10601b8cd6fbda02"
let token = "15af4e61d125b5a711648f6555d989aae5a020a322164fb39989bfae81ceb42a"
let boardid = "DRjF0EnK"
let listid = "5beaaf19a2b06180b474aa74"
let secret = "mg5KZBhQbqHSybfG6g2mhMOJaHJcWtFSQCIsOLO36Ymh7MN9At14I4nGNUW5biUU"
let request = require('request')
let executionurl = ""

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');
    if (req.query.secret == secret && (req.body && req.body.taskname)) {

        executionurl = "https://api.trello.com/1/cards?idList=" + listid + "&name=" + req.body.taskname + "&key=" + apikey + "&token=" + token;
        request.post(executionurl);

        context.res = {
            // status: 200, /* Defaults to 200 */
            body: "Task \"" + req.body.taskname + "\" has been created"
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Please ensure you pass all parameters for creating a task and that your deployment secret is correct"
        };
    }
};